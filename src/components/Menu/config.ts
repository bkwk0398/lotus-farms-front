import { MenuEntry } from '@pancakeswap-libs/uikit'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Exchange',
        href: 'http://exchange.lotusfinance.org/',
      },
      {
        label: 'Liquidity',
        href: 'http://exchange.lotusfinance.org/#/pool',
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Ponds',
    icon: 'PoolIcon',
    href: '/nests',
  },
  // {
  //   label: 'Pools',
  //   icon: 'PoolIcon',
  //   href: '/pools',
  // },
  // {
  //   label: 'Lottery',
  //   icon: 'TicketIcon',
  //   href: '/lottery',
  // },
  // {
  //   label: 'NFT',
  //   icon: 'NftIcon',
  //   href: '/nft',
  // },
  {
    label: 'Info',
    icon: 'InfoIcon',
    items: [
      {
        label: 'PancakeSwap',
        href: 'https://pancakeswap.info/token/0x8c1c63b0c27feb84f895c36d05b0f6236adfb3b8',
      },
      {
        label: 'CoinGecko',
        href: 'https://www.coingecko.com/en/coins/goose-finance',
      },
      {
        label: 'Docs',
        href: 'https://coinmarketcap.com/currencies/goose-finance/',
      },
      {
        label: 'Blog',
        href: 'https://app.astrotools.io/pancake-pair-explorer/0x19e7cbecdd23a16dfa5573df54d98f7caae03019',
      },
    ],
  },
  // {
  //   label: 'IFO',
  //   icon: 'IfoIcon',
  //   href: '/ifo',
  // },
  // {
    // label: 'More',
    // icon: 'MoreIcon',
    // items: [
      // // {
      // //   label: 'Voting',
      // //   href: 'https://voting.pancakeswap.finance',
      // // },
      // {
        // label: "Github",
        // href: "https://github.com/goosedefi/",
      // },
      // {
        // label: "Docs",
        // href: "https://goosedefi.gitbook.io/goose-finance/",
      // },
      // {
        // label: "Blog",
        // href: "https://goosefinance.medium.com/",
      // },
    // ],
  // },
]

export default config
